//
//  Common.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/24.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation


//MARK: 适配iPhone X的导航栏和tabbar
var kTabBarHeight: CGFloat {
    get {
        return kIsiPhoneX ? 83 : 49
    }
}

var kNavBarHeight: CGFloat {
    get {
        return kIsiPhoneX ? 88 : 64
    }
}

/// 判断机型是否是iPhoneX
let kIsiPhoneX = UIScreen.main.bounds.size.height == 812.0




//MARK: 设备屏幕尺寸
let kScreenW = UIScreen.main.bounds.size.width
let kScreenH = UIScreen.main.bounds.size.height

//MARK: 全局颜色


//MARK: 第三方配置信息
let UM_APPKEY =         ""
let WX_APP_SECRET =     ""
let WX_APPID =          ""
let QQ_APPID =          ""
let QQ_APPKEY =         ""
let SINA_APPKEY =       ""
let SINA_APP_SECRET =   ""
let MAP_GAODE_KEY =     ""
let BuglyID =           ""
let STOREAPPID =        ""


/// 判断当前系统版本是否高于指定版本
///
/// - Parameter VersionNum: 指定版本
/// - Returns: 比较值
func iOS_Version_Later(VersionNum: Int) -> Bool {
    let version = UIDevice.current.systemVersion
    let arr = version.components(separatedBy: ".")
    let currentVersion = arr.first
    return Int(currentVersion!) ?? 0 >= Int(VersionNum)
}


/// 自定义debug模式下打印方法
///
/// - Parameters:
///   - message: 内容
///   - fileName: 文件名
///   - methodName: 方法名
///   - lineNumber: 行数
func printLog<N>(_ message:N,fileName:String = #file,methodName:String = #function,lineNumber:Int = #line){
    #if DEBUG
    //获取当前时间
    let now = Date()
    
    // 创建一个日期格式器
    let dformatter = DateFormatter()
    dformatter.dateFormat = "yyyy年MM月dd日 HH:mm:ss"
    print("\(dformatter.string(from: now))   \(fileName as NSString)\n方法:\(methodName)\n行号:\(lineNumber)\n打印信息:\(message) \n" );
    #else
    
    print("方法:\(methodName) 行号:\(lineNumber) 打印信息:\(message) \n" );
    #endif
}


