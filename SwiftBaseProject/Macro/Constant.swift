//
//  Constant.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/24.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation

/**
 全局导入swift框架，避免每个文件里面都要一一导入
 */
@_exported import Kingfisher
@_exported import MJRefresh
@_exported import SnapKit
@_exported import SwiftDate
@_exported import Lottie



//服务器请求地址
let HOSTURL = "http://xatzht.sxxat.com"

let pageSize = 20

let localTestMode = false

enum NotificationEnum: String {
    case LoginNotification
}
