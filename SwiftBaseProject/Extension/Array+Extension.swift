//
//  Array+Extension.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/21.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    /// 删除对象
    ///
    /// - Parameter object: 需要删除的对象<需要遵守可比较协议>
    mutating func remove(_ object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}
 
extension Array {

    /// 将数组转成json字符串
    ///
    /// - Returns: json字符串
    func toJsonString() -> (String) {
        let jsonData = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        if let data = jsonData {
            let str = String.init(data: data, encoding: .utf8)
            return str!
        }
        return ""
    }
}


