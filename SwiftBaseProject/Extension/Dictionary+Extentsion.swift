//
//  Dictionary+Extentsion.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/21.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation


extension Dictionary {
    
    /// 将字段转成json字符串
    ///
    /// - Returns: josn字符串
    func toJsonString() -> (String) {
        let jsonData = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        if let data = jsonData {
            let str = String.init(data: data, encoding: .utf8)
            return str!
        }
        return ""
    }
    
    /// 将字段转成OC字典
    ///
    /// - Returns: NSDictionary
    func toOCDict() -> [NSObject : AnyObject] {
        let ocDict = self as! [NSObject : AnyObject]
        return ocDict
    }
}

