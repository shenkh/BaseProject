//
//  UIButton+Extension.swift
//  SXSchool-Teacher
//
//  Created by 苏州巨细 on 2018/7/2.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation
import Kingfisher

extension UIButton {
    
    /// 便捷设置UIButton图片方法
    ///
    /// - Parameters:
    ///   - url: 图片地址
    ///   - state: 按钮状态
    ///   - placeImage: 占位图
    func kf_setImage(with urlString: String?,
                     state :UIControlState,
                     placeImage: String? = "") -> () {
        
        let url = urlString.flatMap( { URL.init(string: $0) })
        self.kf.setImage(with: url, for: state, placeholder: UIImage.init(named: placeImage!))
    }
    
    /// 便捷设置UIButton背景图片方法
    ///
    /// - Parameters:
    ///   - url: 图片地址
    ///   - state: 按钮状态
    ///   - placeImage: 占位图
    func kf_setBackgroundImage(with urlString: String?,
                               state :UIControlState,
                               placeImage: String? = "") -> () {
        
        let url = URL.init(string: urlString ?? "")
        self.kf.setBackgroundImage(with: url, for: state, placeholder: UIImage.init(named: placeImage!))
    }
}
