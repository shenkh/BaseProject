//
//  UIAlertController+Extension.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/21.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    /// 展示提示框类型
    ///
    /// - OnlyConfirmButton: 只有一个确定按钮
    /// - CustomButtonWithCancelButton: 自定义+取消按钮
    /// - CustomButton: 全部自定义按钮
    /// - ActionSheet: 系统的ActionSheet
    enum ShowAlertType {
        case OnlyConfirmButton
        case CustomButtonWithCancelButton
        case CustomButton
        case ActionSheet
    }
    
    
    /// 展示系统的AlertView
    ///
    /// - Parameters:
    ///   - showType: 展示提示类型
    ///   - title: 展示标题
    ///   - message: 展示内容
    ///   - controller: 调用viewController
    ///   - actionTitleArr: 按钮标题数组
    ///   - actionHandle: 按钮点击事件闭包
    class func showMsgWithAlertView(showType: ShowAlertType,
                                    title: String?,
                                    message: String?,
                                    andController controller: UIViewController,
                                    actionTitleArr: Array<String>?,
                                    actionHandle: ((UIAlertAction) -> Swift.Void)? = nil) -> () {
        
        var alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        switch showType {
        case .OnlyConfirmButton:
            let button = UIAlertAction.init(title: "确定", style: .default, handler: actionHandle)
            alertController.addAction(button)
            
        case .CustomButtonWithCancelButton:
            let cancelButton = UIAlertAction.init(title: "取消", style: .default, handler: nil)
            alertController.addAction(cancelButton)
            
            if let titleArr = actionTitleArr {
                for actionTitle in titleArr {
                    let button = UIAlertAction.init(title: actionTitle, style: .default, handler: actionHandle)
                    alertController.addAction(button)
                }
                
            }
            break
            
        case .CustomButton:
            if let titleArr = actionTitleArr {
                for actionTitle in titleArr {
                    let button = UIAlertAction.init(title: actionTitle, style: .default, handler: actionHandle)
                    alertController.addAction(button)
                }
            }
            break
            
        case .ActionSheet:
            alertController = UIAlertController.init(title: title, message: message, preferredStyle: .actionSheet)
            
            if let titleArr = actionTitleArr {
                for actionTitle in titleArr {
                    let button = UIAlertAction.init(title: actionTitle, style: .default, handler: actionHandle)
                    alertController.addAction(button)
                }
            }
            
            let cancelButton = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
            alertController.addAction(cancelButton)
            
            break
        }
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    
    /// 展示系统的actionAlert
    /// - Parameters:
    ///   - title: 标题
    ///   - message: 信息
    ///   - controller: 调用的controller
    ///   - actionArr: UIAlertAction数组
    class func showMsgWithActionView(title: String?,
                                     message: String?,
                                     andController controller: UIViewController,
                                     actionArr: [UIAlertAction]?) -> () {
        let alertViewController = UIAlertController.init(title: title, message: message, preferredStyle: .actionSheet)
        if let actionArr = actionArr {
            for actionButton in actionArr {
                alertViewController.addAction(actionButton);
            }
        }
        let cancelButton = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        alertViewController.addAction(cancelButton)
        controller.present(alertViewController, animated: true, completion: nil)
    }
    
}
