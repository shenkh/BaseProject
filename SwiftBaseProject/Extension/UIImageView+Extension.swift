//
//  UIImageView+Extension.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/22.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    /// 便捷设置UIImageView图片方法
    ///
    /// - Parameters:
    ///   - urlString: 图片url地址
    ///   - placeImage: 占位图
    func kf_setImage(with urlString: String?, placeImage: String? = "", isNeedAddFullPath: Bool = true) -> () {
        
        var url: URL?
        if isNeedAddFullPath {
            url = URL.init(string: "需要拼接的全地址")
        }else{
            url = URL.init(string: urlString ?? "")
        }
        
        /// 查看flatmap的用法，过滤掉nil。  等于 let url = URL.init(string: urlString ?? "")
//        let url = urlString.flatMap( { URL.init(string: $0) })
        self.kf.setImage(with: url, placeholder: UIImage.init(named: placeImage!), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
}


