//
//  UIImage+Extension.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/22.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation



extension UIImage {
    
    /// 根据颜色来生成图片
    ///
    /// - Parameters:
    ///   - color: 图片颜色
    ///   - size: 图片尺寸
    class func initWithColor(_ color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) -> (UIImage) {
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        UIGraphicsBeginImageContext(rect.size)
        
        guard let imageContext = UIGraphicsGetCurrentContext() else {
            return UIImage()
        }
        
        imageContext.setFillColor(color.cgColor)
        imageContext.fill(rect)
        
        guard let resultImage = UIGraphicsGetImageFromCurrentImageContext() else {
            return UIImage()
        }
        
        UIGraphicsEndImageContext()
        
        return resultImage
    }
}
