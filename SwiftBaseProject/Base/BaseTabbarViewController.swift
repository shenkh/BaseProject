//
//  BaseTabbarViewController.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/25.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation

class BaseTabbarViewController: UITabBarController {
    
    override func viewDidLoad() {
        
        let vc = setupChildViewController(navVCName: BaseNavigationViewController.self, childVC: ViewController(), title: "首页", normalImageName: "tabBar_wo", selectedImageName: "tabBar_wo_sel")
        self.addChildViewController(vc)
        
        let secondVC = setupChildViewController(navVCName: BaseNavigationViewController.self, childVC: SecondViewController(), title: "消息", normalImageName: "tabBar_wo", selectedImageName: "tabBar_wo_sel")
        self.addChildViewController(secondVC)
        
        
        //添加背景图片，为解决12.1系统，tabbarItem图标漂移
        self.tabBar.backgroundImage = UIImage.initWithColor(UIColor.white)
    }
    
    
}

extension BaseTabbarViewController: TabbarProtocol {
    
    var normalTitleColor: UIColor {
        return UIColor.black
    }
    
    var selectedTitleColor: UIColor {
        return UIColor.red
    }
    
    var titleFont: UIFont {
        return UIFont.systemFont(ofSize: 16)
    }
    
}

