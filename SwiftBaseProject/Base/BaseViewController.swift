//
//  BaseViewController.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/18.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class BaseViewController: UIViewController {
    
    
    /// 导航栏标题
    private(set) var navTitleLabel: UILabel?
    
    /// 导航栏返回按钮
    private(set) var backBarBtn: UIButton?
    
    /// 当前页码
    var pageIndex: NSInteger = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        intoViewControllerInfo()
        
        initData()
        initUI()
        
        // Do any additional setup after loading the view.
    }
    
    /// 初始化数据方法归类
    func initData() -> () {
        printLog("初始化数据")
    }
    
    /// 初始化界面方法归类
    func initUI() -> () {
        printLog("初始化界面")
        view.backgroundColor = UIColor.white
        if let nav = navigationController {
            if nav.childViewControllers.count > 1 {
                base_createLeftBackNavigationBar()
            }
        }
    }
    
    deinit {
        leaveOutViewControllerInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


// MARK: - 导航栏相关
extension BaseViewController {
    
    /// 单独改变导航栏标题文字
    ///
    /// - Parameter title: 标题
    func base_changeNavigationTitleWithString(_ title: String) -> () {
        navTitleLabel = UILabel.init()
        guard let titleLabel = navTitleLabel else {
            return
        }
        titleLabel.numberOfLines = 2
        titleLabel.frame = CGRect(x: 0, y: 0, width: 135, height: 44)
        titleLabel.font = UIFont.systemFont(ofSize: 24)
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.text = title
        
        navigationItem.titleView = titleLabel
    }
    
    /// 创建返回按钮
    func base_createLeftBackNavigationBar() -> () {
        
        base_createLeftNavigationBar(imageName: "global_back")
    }

    /// 创建导航栏左边按钮
    ///
    /// - Parameters:
    ///   - title: 文字
    ///   - imageName: 图片名称
    func base_createLeftNavigationBar(title: String = "", imageName: String = "" ) -> () {
        
        let leftButton = UIButton.init(type: .custom)
        leftButton.frame = CGRect(x: 0, y: 0, width: 90, height: 30)
        leftButton.backgroundColor = UIColor.clear
        leftButton.contentHorizontalAlignment = .left
        leftButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
        leftButton.addTarget(self, action: #selector(base_leftNavigationButtonItemClick), for: .touchUpInside)
        
        if !title.isEmpty {
            leftButton.setTitle(title, for: .normal)
            leftButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            leftButton.setTitleColor(UIColor.black, for: .normal)
        }
        
        if !imageName.isEmpty {
            leftButton.setImage(UIImage.init(named: imageName), for: .normal)
        }
        
         let spaceItem = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spaceItem.width = -10
        let leftItem = UIBarButtonItem.init(customView: leftButton)
        navigationItem.leftBarButtonItems = [spaceItem, leftItem]
    }
    
    /// 创建导航栏右边按钮
    ///
    /// - Parameters:
    ///   - title: 文字
    ///   - imageName: 图片名称
    func base_createRightNavigationBar(title: String = "", imageName: String = "" ) -> () {
        
        let rightButton = UIButton.init(type: .custom)
        rightButton.frame = CGRect(x: 0, y: 0, width: 90, height: 30)
        rightButton.backgroundColor = UIColor.clear
        rightButton.contentHorizontalAlignment = .right
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
        rightButton.addTarget(self, action: #selector(base_rightNavigationButtonItemClick), for: .touchUpInside)
        
        if !title.isEmpty {
            rightButton.setTitle(title, for: .normal)
            rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            rightButton.setTitleColor(UIColor.black, for: .normal)
        }
        
        if !imageName.isEmpty {
            rightButton.setImage(UIImage.init(named: imageName), for: .normal)
        }
        
        let spaceItem = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spaceItem.width = -10
        let rightItem = UIBarButtonItem.init(customView: rightButton)
        navigationItem.rightBarButtonItems = [spaceItem, rightItem]
    }
    
    /// 左边按钮点击事件
    @objc func base_leftNavigationButtonItemClick() -> () {
        printLog("左边按钮点击")
        navigationController?.popViewController(animated: true)
    }
    
    /// 右边按钮点击事件
    @objc func base_rightNavigationButtonItemClick() -> () {
        printLog("右边按钮点击")
    }
    
}

// MARK: - 视图信息
extension BaseViewController {
    
    func intoViewControllerInfo() -> () {
        printLog("--------   进入\(self.classForCoder) ----------")
    }
    
    func leaveOutViewControllerInfo() -> () {
        printLog("--------   销毁\(self.classForCoder) ----------")
    }
}

// MARK: - 空数据页面实现
extension BaseViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    /// 空页面图片
    ///
    /// - Parameter scrollView: 需要实现的空界面
    /// - Returns: UIImage
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage.initWithColor(UIColor.red, size: CGSize(width: 200, height: 200))
    }
    
    /// 设置显示空数据标题信息
    ///
    /// - Parameter scrollView: 需要实现的空界面
    /// - Returns: NSAttributedString
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "空信息"
        let attributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: CGFloat(16.0)),
                          NSAttributedStringKey.foregroundColor: UIColor.black]
        return NSAttributedString(string: text, attributes: attributes as [NSAttributedStringKey : Any])
    }
    
    /// 设置显示空数据详细信息
    ///
    /// - Parameter scrollView: 需要实现的空界面
    /// - Returns: NSAttributedString
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "详细信息"
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .center
        let attributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: CGFloat(13.0)),
                          NSAttributedStringKey.foregroundColor: UIColor.black,
                          NSAttributedStringKey.paragraphStyle:paragraph]
        return NSAttributedString(string: text, attributes: attributes as [NSAttributedStringKey : Any])
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return UIColor.white
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
}

