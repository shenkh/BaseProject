//
//  TabbarViewController+Extension.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/25.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation


protocol TabbarProtocol {
    
    /// 默认标题颜色
    var normalTitleColor: UIColor { get }
    
    /// 选中标题颜色
    var selectedTitleColor: UIColor { get }
    
    /// 标题字体
    var titleFont: UIFont { get }
    
    
    func setupChildViewController(navVCName: AnyClass, childVC: UIViewController, title: String, normalImageName: String, selectedImageName: String) -> (UIViewController)
}


extension TabbarProtocol {
    
    var normalTitleColor: UIColor {
        return UIColor.gray
    }
    
    var selectedTitleColor: UIColor {
        return UIColor.black
    }
    
    var titleFont: UIFont {
        return UIFont.systemFont(ofSize: 15)
    }
    
    func setupChildViewController(navVCName: AnyClass, childVC: UIViewController, title: String, normalImageName: String, selectedImageName: String) -> (UIViewController) {
        
        childVC.tabBarItem.title = title
        
        childVC.tabBarItem.selectedImage = UIImage.init(named: selectedImageName)?.withRenderingMode(.alwaysOriginal)
        childVC.tabBarItem.image = UIImage.init(named: normalImageName)?.withRenderingMode(.alwaysOriginal)
        childVC.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
        childVC.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font : titleFont], for: .normal)
        childVC.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : selectedTitleColor], for: .selected)
        childVC.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : normalTitleColor], for: .normal)

        let classType = navVCName as? UINavigationController.Type
        let navVC = classType?.init(rootViewController: childVC)
        
        return navVC!
    }
    
}
