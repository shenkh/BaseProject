//
//  BaseNavigationViewController.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/25.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation


class BaseNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        
        printLog("创建导航器")
        
        //创建图片，不然会有缩进问题
        self.navigationBar.setBackgroundImage(UIImage.initWithColor(UIColor.colorWithHexString("#fafafa")), for: .default)
        self.navigationBar.shadowImage = UIImage.init()
        
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if self.childViewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: true)
    }
    
}
