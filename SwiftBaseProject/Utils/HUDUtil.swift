//
//  HUDUtil.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/8/9.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import UIKit
import SVProgressHUD

class HUDUtil {

    /// 显示错误信息HUD
    ///
    /// - Parameter error: 错误信息
    class func showError(_ error: String) -> () {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setMinimumDismissTimeInterval(0.8)
        SVProgressHUD.setBackgroundColor(UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.8))
        SVProgressHUD.setForegroundColor(UIColor.white)
        
        if let image = UIImage.init(named: "error") {
            SVProgressHUD.setImageViewSize(image.size)
            SVProgressHUD.setErrorImage(image)
        }else{
            SVProgressHUD.setSuccessImage(UIImage.initWithColor(Color.black))
            SVProgressHUD.setImageViewSize(CGSize.init(width: 0.0, height: 0.0))
        }
        SVProgressHUD.showError(withStatus: error)
    }
    
    /// 显示成功信息HUD
    ///
    /// - Parameter success: 成功信息
    class func showSuccess(_ success: String) -> () {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setMinimumDismissTimeInterval(0.8)
        SVProgressHUD.setBackgroundColor(UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.8))
        SVProgressHUD.setForegroundColor(UIColor.white)
        
        if let image = UIImage.init(named: "success") {
            SVProgressHUD.setImageViewSize(image.size)
            SVProgressHUD.setSuccessImage(image)
        }
        else{
            SVProgressHUD.setSuccessImage(UIImage.initWithColor(Color.black))
            SVProgressHUD.setImageViewSize(CGSize.init(width: 0.0, height: 0.0))
        }
        SVProgressHUD.showSuccess(withStatus: success)
    }
    
    /// 显示信息HUD
    ///
    /// - Parameters:
    ///   - message: 提示信息
    ///   - time: 多少s后消失
    class func showMessage(_ message: String, withDelay time: TimeInterval = 0.8) -> () {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setMinimumDismissTimeInterval(0.8)
        SVProgressHUD.setBackgroundColor(UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.8))
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setDefaultMaskType(.black)
        if message.count <= 0 {
            SVProgressHUD.show()
        }else{
            SVProgressHUD.show(withStatus: message)
        }
        SVProgressHUD.dismiss(withDelay:time)
    }
    
    /// 隐藏信息HUD
    ///
    /// - Parameter time: 多少s后消失
    class func hiddenHUD(_ time: TimeInterval = 0.0) -> () {
        SVProgressHUD.dismiss(withDelay:time)
    }
    
}
