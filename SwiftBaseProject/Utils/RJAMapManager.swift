//
//  RJAMapManager.swift
//  SXSchool-Parent
//
//  Created by 苏州巨细 on 2018/7/19.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import UIKit
import CoreLocation


class RJLocationModel: NSObject {
    
    /// 经度
    var longitude: Double?
    /// 纬度
    var latitude: Double?
    /// 时间
    var time: Double?
    /// 速度
    var speed: Double?
    /// 角度
    var angle: Double?
    
    var reGeocode: AMapLocationReGeocode?
    
    override init() {
        super.init()
    }
    
}


class RJAMapManager: NSObject {

    static let shareInstance = RJAMapManager()
    
    var locationModel = RJLocationModel()
    
    private var locationManager: AMapLocationManager?
    
    private var locationBlock: ((_ locationModel: RJLocationModel) -> Void)?
    
    override init() {
        
        locationManager = AMapLocationManager.init()
        super.init()
        guard let locationManager = locationManager else { return }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
    }
    
    /// 开启单次定位
    ///
    /// - Parameter completion: 回调事件
    func startLocationOnceWithSuccess(completion: @escaping (RJLocationModel) -> Void) -> () {
        self.locationBlock = completion
        guard let locationManager = locationManager else { return }
        locationManager.locationTimeout = 10
        locationManager.reGeocodeTimeout = 5
        
        startLocation()
    }
    
    /// 单次定位
    func startLocation() -> () {
        
        self.locationManager!.requestLocation(withReGeocode: true, completionBlock: { (location, locationReGeocode, error) in
            self.locationModel.longitude = location?.coordinate.longitude
            self.locationModel.latitude = location?.coordinate.latitude
            self.locationModel.reGeocode = locationReGeocode
            guard let locationBlock = self.locationBlock else { return }
            locationBlock(self.locationModel)
        })
    }
    
}

extension RJAMapManager: AMapLocationManagerDelegate {
    

}
