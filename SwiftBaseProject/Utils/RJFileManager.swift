//
//  RJFileManager.swift
//  SwiftBaseProject
//
//  Created by monkey_rjpan on 2018/7/20.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import UIKit

class RJFileManager: NSObject {

    /// 新建文件夹（存在则不重复创建）
    ///
    /// - Parameter path: 文件夹路径
    /// - Returns: 是否创建成功
    class func createFolder(path: String) -> (Bool) {
        
        let fileManager = FileManager.default
        do {
            try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
             return true
        }catch let err {
            printLog(err)
            return false
        }
    }
    
    /// 删除指定文件
    ///
    /// - Parameter path: 文件路径
    class func deleteFilePath(path:String){
        
        let fileManager = FileManager.default
        
        do{
            try fileManager.removeItem(atPath: path)
        } catch{
            printLog("删除失败")
        }
    }
    
    
    /// 用户文档目录，苹果建议将程序中建立的或在程序中浏览到的文件数据保存在该目录下，iTunes备份和恢复的时候会包括此目录
    ///
    /// - Returns: 文件夹路径
    class func documentDirectoryPath() -> (String) {
       return NSHomeDirectory() + "/Documents"
    }
    
    /// 获取缓存文件夹路径
    /// 这个目录下有两个子目录：Caches 和 Preferences
    /// Library/Preferences目录，包含应用程序的偏好设置文件。不应该直接创建偏好设置文件，而是应该使用NSUserDefaults类来取得和设置应用程序的偏好。
    /// Library/Caches目录，主要存放缓存文件，iTunes不会备份此目录，此目录下文件不会再应用退出时删除
    /// - Returns: 缓存路径
    class func cachesDirectoryPath() -> (String) {
        return NSHomeDirectory() + "/Library/Caches"
    }
    
    /// 用于存放临时文件，保存应用程序再次启动过程中不需要的信息，重启后清空。
    ///
    /// - Returns: 临时文件夹路径
    class func tempDirectoryPath() -> (String) {
        return NSHomeDirectory() + "/tmp"
    }
    
}
