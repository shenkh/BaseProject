//
//  SecondViewController.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/25.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation


class SecondViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.red
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationEnum.LoginNotification.rawValue), object: nil)
    }
    
    override func initUI() {
        super.initUI()
        self.title = "消息"
        self.base_changeNavigationTitleWithString("第二")
        
        self.base_createRightNavigationBar(title: "会话", imageName: "")
    }
    
    override func base_rightNavigationButtonItemClick() {
        
        
    }
    
    
}
