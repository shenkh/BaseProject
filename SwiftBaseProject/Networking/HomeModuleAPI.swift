//
//  HomeModuleAPI.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/22.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation
import Moya

enum HomeModuleHttp {
    case homeData([String : Any])
    case imageArr([String : Any])
}

extension HomeModuleHttp: TargetType{
//    public var validate: Bool {
//        return false
//    }
//
    //请求URL
    public var baseURL:URL{
        return URL(string:"http://wl.51zhaobu.com")!
    }
    //详细的路径(例如/login)
    public var path: String {
        switch self {
        case .homeData,.imageArr:
            return "/driver/login"
        }
    }
    ///请求方式
    public var method:Moya.Method {
        switch self {
        case .homeData,.imageArr:
            return .post
        }
    }
    ///单元测试用
    public var sampleData:Data{
        return "".data(using:.utf8)!
    }
    ///任务,这边可以设置请求参数编码，一般使用JSONEncoding
    public var task: Task {
        switch self {
        case let .homeData(params):
            return .requestParameters(parameters:params, encoding: JSONEncoding.default)
        case let .imageArr(params):
            return .requestParameters(parameters:params, encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    
    }
    
    
    ///请求头信息
//    public var headers: [String : String]? {
//        return [
//            "OS":"2"
//        ]
//    }
    
}


