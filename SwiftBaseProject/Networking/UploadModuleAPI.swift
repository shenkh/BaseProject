//
//  UploadModuleAPI.swift
//  SXSchool-Parent
//
//  Created by 苏州巨细 on 2018/7/27.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import UIKit

import Moya

enum UploadModuleHttp {
    case uploadImage(image: UIImage)//上传图片
}


extension UploadModuleHttp: TargetType{
    
    var baseURL: URL {
        if localTestMode {
            switch self {
            case .uploadImage:
                return URL.init(string: "")!
            }
        }else{
            //域名问题，先用IP地址访问
            return URL.init(string: "")!
        }
    }
    
    //详细的路径(例如/login)
    public var path: String {
        if localTestMode {
            return ""
        }else{
            switch self {
            case .uploadImage:
                return ""
            }
        }
    }
    
    ///请求方式
    public var method:Moya.Method {
        return .post
    }
    
    ///单元测试用
    public var sampleData:Data{
        return "".data(using:.utf8)!
    }
    
    ///任务,这边可以设置请求参数编码，一般使用JSONEncoding
    public var task: Task {
        switch self {
        case let .uploadImage(image: image):
            
            let imageData = UIImageJPEGRepresentation(image, 0.8)
        
            guard let data = imageData else {
                HUDUtil.showSuccess("无效图片")
                return .requestPlain
            }
            
            let time = Date().toFormat("yyyyMMddhhmmss")
            
            let uploadData = MultipartFormData.init(provider: .data(data), name: time, fileName: time+".jpg", mimeType: "image/jpeg")
            
            return .uploadMultipart([uploadData])
        default:
            return .requestPlain
        }
        
    }
    
}
