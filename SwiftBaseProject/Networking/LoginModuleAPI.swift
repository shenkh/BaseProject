//
//  LoginModuleAPI.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/22.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation
import Moya


enum LoginModuleHttp {
    case login(account:String,password:String)
}


extension LoginModuleHttp: TargetType{
    
    //详细的路径(例如/login)
    public var path: String {
        switch self {
        case .login(_,_):
            return "/user/logintt"
        }
    }
    ///请求方式
    public var method:Moya.Method {
        switch self {
        case .login(_,_):
            return .get
        }
    }
    ///单元测试用
    public var sampleData:Data{
        return "".data(using:.utf8)!
    }
    ///任务
    public var task: Task {
        switch self {
        case let .login(account,password):
            return .requestParameters(parameters:["account":account,"password": password], encoding: URLEncoding.default)
        }
    }
    
}
