//
//  BaseModuleAPI.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/25.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation
import Moya


extension TargetType {
    
    public var validate: Bool {
        return false
    }
    
    //请求URL值
    public var baseURL:URL{
        return URL(string:"http://wl.51zhaobu.com")!
    }
    
    ///请求头信息(子类可复写)
    public var headers: [String : String]? {
        return [
            "OS":"2"
        ]
    }
    
    /// 请在子类中复写定义
    public var showHUD: Bool {
        return true
    }

    
}
