//
//  Cat.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/24.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import Foundation
import HandyJSON

enum CarType: Int {
    case Bike = 1
    case Car = 2
}

class Zoom: HandyJSON {
    var name : String?
    var ani: Cat?
    var type: CarType?
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
            type <-- TransformOf<CarType, String>(fromJSON: { (rawString) -> CarType? in
                if let _str = rawString {
                    switch (_str) {
                    case "1":
                        return CarType.Bike
                    case "2":
                        return CarType.Car
                    default:
                        return nil
                    }
                }
                return nil
            }, toJSON: { (carType) -> String? in
                if let _type = carType {
                    switch (_type) {
                    case CarType.Bike:
                        return "1"
                    case CarType.Car:
                        return "2"
                    }
                }
                return nil
            })
    }
    
    required init () {
        
    }
    
}


class Cat: HandyJSON {
    
    var name: String?
    
    var code: String?
    
    var reason: String?
    
    required init() {
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
            self.name <-- "color"
    }
    
}
