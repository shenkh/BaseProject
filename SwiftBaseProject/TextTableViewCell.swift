//
//  TextTableViewCell.swift
//  SwiftBaseProject
//
//  Created by 苏州巨细 on 2018/5/17.
//  Copyright © 2018年 苏州巨细. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell {
    
    let menuController = UIMenuController.shared
    
    var deleteMenuItem: UIMenuItem?
    
    var menuIndexPath: IndexPath?

    public var nameContent : String?
    {
        didSet{
            nameLabel.text = nameContent
        }
    }
    
    public func setNameContent(_ content: String) -> () {
        nameContent = content
        nameLabel.text = content
    }
    
    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(contentView).offset(20)
            make.left.equalTo(contentView).offset(20)
            make.right.equalTo(contentView).offset(-20)
            make.bottom.equalTo(contentView).offset(-20)
        }
        
//        contentView.snp.makeConstraints { (make) in
//            make.bottom.equalTo(nameLabel.snp.bottom).offset(20)
//            make.left.equalTo(self)
//            make.right.equalTo(self)
//            make.top.equalTo(self)
//        }
        
    }
    

    
    
    override var canBecomeFirstResponder: Bool {
        return true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
