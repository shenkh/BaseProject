# BaseProject

#### 项目介绍
Swift搭建的混合开发基础框架（主要以swift语言为主，暂时找不到优秀的Swift第三方继续使用OC框架代替），进行了一些简单的第三使用示例，整理了一些常用工具，以后将用于项目开发，并持续完善。希望在此项目中对Swift4有新的了解，尝试更多有趣的实现功能方法，方便项目维护，和日常更新。

#### 软件架构
Hello Swift


#### 安装教程

1. 请下载源码进行查看。

#### 使用说明

1. 请下载源码进行查看。
2. 下载完成记得 pod install
3. 问题请走Issues

#### 参与贡献

1. 无


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)